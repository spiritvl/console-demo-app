<?php

namespace App;

use App\Command\DumpCommand;
use RuntimeException;
use Spiritvl\Console\Console;

class Application
{
    private Console $console;

    public function __construct()
    {
        $this->console = new Console([
            new DumpCommand(),
        ]);
    }

    public function run(array $argv): string
    {
        try {
            return $this->console->handleInput($argv);
        } catch (RuntimeException $exception) {
            return 'ERROR: ' . $exception->getMessage() . PHP_EOL;
        }
    }
}
