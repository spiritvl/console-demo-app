<?php

namespace App\Command;

use Spiritvl\Console\CommandInterface;
use Spiritvl\Console\Input\Parameters\ParametersBag;
use Spiritvl\Console\Output\StringBuffer;

class DumpCommand implements CommandInterface
{
    public function name(): string
    {
        return 'dump';
    }

    public function description(): string
    {
        return 'Dump all arguments and options.';
    }

    public function run(ParametersBag $parameters): string
    {
        $buffer = new StringBuffer(['Arguments:']);

        foreach($parameters->arguments()->all() as $argument) {
            $buffer->addLine("\t- " . $argument);
        }

        $buffer->addLine('Options:');

        foreach($parameters->options()->all() as $optionName => $optionValue) {
            $buffer->addLine("\t- " . $optionName);
            if (is_array($optionValue)) {
                foreach ($optionValue as $option) {
                    $buffer->addLine("\t\t- " . $option);
                }
            } else {
                $buffer->addLine("\t\t- " . $optionValue);
            }
        }

        return $buffer->toString();
    }
}
