# Demo-приложение

Приложение, использующее библиотеку `spiritvl\console`

# Локальная установка

* Запустить `docker-compose up -d`

# Запуск приложения

* Список всех команд: `php app.php`
* Вывод описания команды `dump`: `php app.php dump {help}`
* Вывод всех параметров: `php app.php dump {arg} [name=value] {arg2,arg3} [age=16,18,21]`
